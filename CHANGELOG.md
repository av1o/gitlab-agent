## 17.0.0 (2024-05-15)

### Added (2 changes)

- [Generate Ruby bindings for Server Info RPC](gitlab-org/cluster-integration/gitlab-agent@a4e72b4703a164534aabca74f4b81662276969b5) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1455))
- [Implement Server Info RPC interface](gitlab-org/cluster-integration/gitlab-agent@1f08c0d278f685b59b44231ac2ed5856f4fa7305) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1455))

### Fixed (2 changes)

- [Do not allow to inject version from outside](gitlab-org/cluster-integration/gitlab-agent@1dd0ece91d724bc090c3e3b88433883b906bd96d) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1470))
- [Use -u instead of --utc for date command](gitlab-org/cluster-integration/gitlab-agent@b18d2a66a3280f12ff6049b0678360bf45ca0ace) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1469))

### Changed (4 changes)

- [Drop sigs.k8s.io/controller-runtime dependency](gitlab-org/cluster-integration/gitlab-agent@298927f4512163308de680121eb7cd2937db7223) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1500))
- [Bump Go dependencies](gitlab-org/cluster-integration/gitlab-agent@59f397dde4a8fdd08ede5dc8b40e58686a97df0b) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1500))
- [Use standard datetime format for build time info](gitlab-org/cluster-integration/gitlab-agent@b429f282f0e5ff65686c865814abb1c1eb060b00) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1466))
- [Increase max size of Agent Meta Version string](gitlab-org/cluster-integration/gitlab-agent@203e537e74e1e0bec8ac3231e2a1eb6b607fbeec) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1457))

### Removed (3 changes)

- [Remove GetConnectedAgents gRPC service](gitlab-org/cluster-integration/gitlab-agent@2f349b55ad3b1473d11a477f49ea61e72e584b91) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1433))
- [Remove deprecated gRPC services](gitlab-org/cluster-integration/gitlab-agent@deaaa02c20861140917c184b30ec10fcf1ae3c8c) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1433))
- [Remove `ca-cert-file` CLI option in agentk](gitlab-org/cluster-integration/gitlab-agent@c5ee86f8c38171f20a6347eabff358361b85e657) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1431))

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/cluster-integration/gitlab-agent@19674f3d5a629086abafa2ec93922c2818ef8139) ([merge request](gitlab-org/cluster-integration/gitlab-agent!1434))

## 16.11.2 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@141707b32694692d495979eb3191004de3964962)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@ebe741712fd4b178668c3485e1bd99cc041a14f9)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@aefc4f5a3513fb0d67283ec3d11649a601977d5d)

## 16.11.1 (2024-04-24)

No changes.

## 16.11.0 (2024-04-17)

### Added (1 change)

- [Opt-out option for Flux receiver creation](gitlab-org/cluster-integration/gitlab-agent@81485a3c96ec78b9fb1d96b053669fb467b6fa65)

### Changed (4 changes)

- [Tune Redis COUNT modifier for HSCAN](gitlab-org/cluster-integration/gitlab-agent@2cda802f2e292751aab4ed169ff6961b9281ac54)
- [Only reconcile GitRepository and Receiver when spec changes](gitlab-org/cluster-integration/gitlab-agent@389c01c15c21c1cb1801a5a121d696fc9d8a2890)
- [ScanAndGC -> independent Scan + GC](gitlab-org/cluster-integration/gitlab-agent@49938bd26b220dadd4da9644eb278e01e8b0fe23)
- [Update dependency trivy-k8s-wrapper to v0.2.15](gitlab-org/cluster-integration/gitlab-agent@acce190f70c52c701f1d64248305af47ed1023b3)

### Fixed (4 changes)

- [Stop erroring out when OWN_PRIVATE_API_HOST is not set](gitlab-org/cluster-integration/gitlab-agent@fad847403ffe99cf1cd7df0d6656a40a1737d2e0)
- [Fix garbage collection for keys registered with SetEX](gitlab-org/cluster-integration/gitlab-agent@3fd1c3df686fc07db7fba0ab98c3bf6a7490703b)
- [Fix KAS shutdown issue](gitlab-org/cluster-integration/gitlab-agent@9ace31f290dbef83724abb48d3665cf80f41a083)
- [Don't panic if module exits when config channel is closed](gitlab-org/cluster-integration/gitlab-agent@04d8e67a63c900e68e6aa5bd0f162d20ac3bc643)

## 16.10.5 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@cdcf385865c6fb2603f5f4e35cb1199315609274)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@516f2f3232573e8d1eedf2e66e21258a53ba1f2a)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@0b1112a816876ac08e7161084749aadf4cdf88c7)

## 16.10.4 (2024-04-24)

No changes.

## 16.9.8 (2024-05-09)

No changes.

## 16.9.7 (2024-05-07)

### Fixed (1 change)

- [Do not allow to inject version from outside](gitlab-org/security/cluster-integration/gitlab-agent@ccfbedc0b0509fe37250042744d578bc3491325a)

### Changed (1 change)

- [Increase max size of Agent Meta Version string](gitlab-org/security/cluster-integration/gitlab-agent@37a22cc5e72f82f5168314fd62a6734a232ede9c)

### Other (1 change)

- [Introduce support for VERSION file](gitlab-org/security/cluster-integration/gitlab-agent@de9d1e4ea520890b8531335dd006c8c7e3c9c144)

## 16.9.6 (2024-04-24)

No changes.
