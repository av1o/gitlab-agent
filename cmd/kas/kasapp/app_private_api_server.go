package kasapp

import (
	"context"
	"errors"
	"fmt"
	"math"
	"net"
	"os"
	"strconv"
	"time"

	"github.com/ash2k/stager"
	"github.com/golang-jwt/jwt/v5"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/validator"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/observability"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/ioz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/nettool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/tlstool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/backoff"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/stats"
)

const (
	envVarOwnPrivateAPIURL    = "OWN_PRIVATE_API_URL"
	envVarOwnPrivateAPICIDR   = "OWN_PRIVATE_API_CIDR"
	envVarOwnPrivateAPIScheme = "OWN_PRIVATE_API_SCHEME"
	envVarOwnPrivateAPIPort   = "OWN_PRIVATE_API_PORT"
	envVarOwnPrivateAPIHost   = "OWN_PRIVATE_API_HOST"
)

type privateAPIServer struct {
	log           *zap.Logger
	listenCfg     *kascfg.ListenPrivateApiCF
	ownURL        string
	server        grpctool.GRPCServer
	listenServer  *grpc.Server
	inMemServer   *grpc.Server
	inMemListener net.Listener
	kasPool       grpctool.PoolInterface
	auxCancel     context.CancelFunc
	ready         func()
}

func newPrivateAPIServer(log *zap.Logger, errRep errz.ErrReporter, cfg *kascfg.ConfigurationFile, tp trace.TracerProvider,
	mp otelmetric.MeterProvider, p propagation.TextMapPropagator, csh, ssh stats.Handler, factory modshared.RPCAPIFactory,
	probeRegistry *observability.ProbeRegistry,
	streamProm grpc.StreamServerInterceptor, unaryProm grpc.UnaryServerInterceptor,
	streamClientProm grpc.StreamClientInterceptor, unaryClientProm grpc.UnaryClientInterceptor,
	grpcServerErrorReporter grpctool.ServerErrorReporter) (*privateAPIServer, error) {
	listenCfg := cfg.PrivateApi.Listen
	jwtSecret, err := ioz.LoadBase64Secret(listenCfg.AuthenticationSecretFile)
	if err != nil {
		return nil, fmt.Errorf("auth secret file: %w", err)
	}

	ownURL, err := constructOwnURL(
		net.InterfaceAddrs,
		os.Getenv(envVarOwnPrivateAPIURL),
		os.Getenv(envVarOwnPrivateAPICIDR),
		os.Getenv(envVarOwnPrivateAPIScheme),
		os.Getenv(envVarOwnPrivateAPIPort),
		*listenCfg.Network,
		listenCfg.Address,
	)
	if err != nil {
		return nil, err
	}
	log.Info("Using own private API URL", logz.URL(ownURL))

	ownHost := os.Getenv(envVarOwnPrivateAPIHost)

	// In-memory gRPC client->listener pipe
	listener := grpctool.NewDialListener()

	// Client pool
	kasPool, err := newKASPool(log, errRep, tp, mp, p, csh, jwtSecret, ownURL, ownHost,
		listenCfg.CaCertificateFile, listener.DialContext, streamClientProm, unaryClientProm)
	if err != nil {
		return nil, fmt.Errorf("kas pool: %w", err)
	}

	// Server
	auxCtx, auxCancel := context.WithCancel(context.Background()) //nolint: govet
	server, inMemServer, err := newPrivateAPIServerImpl(log, auxCtx, cfg, tp, mp, p, ssh, jwtSecret, factory, ownHost, streamProm, unaryProm, grpcServerErrorReporter)
	if err != nil {
		return nil, fmt.Errorf("new server: %w", err) //nolint: govet
	}
	return &privateAPIServer{
		log:           log,
		listenCfg:     listenCfg,
		ownURL:        ownURL,
		server:        grpctool.AggregateServer{server, inMemServer},
		listenServer:  server,
		inMemServer:   inMemServer,
		inMemListener: listener,
		kasPool:       kasPool,
		auxCancel:     auxCancel,
		ready:         probeRegistry.RegisterReadinessToggle("privateAPIServer"),
	}, nil
}

func (s *privateAPIServer) Start(stage stager.Stage) {
	stopInMem := make(chan struct{})
	grpctool.StartServer(stage, s.inMemServer, func() (net.Listener, error) {
		return s.inMemListener, nil
	}, func() {
		<-stopInMem
		err := s.kasPool.Close()
		if err != nil {
			s.log.Error("Failed to close KAS connection pool", logz.Error(err))
		}
	})
	grpctool.StartServer(stage, s.listenServer, func() (net.Listener, error) {
		lis, err := nettool.ListenWithOSTCPKeepAlive(*s.listenCfg.Network, s.listenCfg.Address)
		if err != nil {
			return nil, err
		}
		addr := lis.Addr()
		s.log.Info("Private API endpoint is up",
			logz.NetNetworkFromAddr(addr),
			logz.NetAddressFromAddr(addr),
		)
		s.ready()
		return lis, nil
	}, func() {
		time.Sleep(s.listenCfg.ListenGracePeriod.AsDuration())
		// We first want gRPC server to send GOAWAY and only then return from the RPC handlers.
		// So we delay signaling the handlers.
		// See https://github.com/grpc/grpc-go/issues/6830 for more background.
		// Start a goroutine in a second and...
		time.AfterFunc(time.Second, func() {
			close(stopInMem) // ... signal the in-memory server to stop.
			s.auxCancel()    // ... signal running RPC handlers to stop.
		})
	})
}

func (s *privateAPIServer) Close() error {
	return errors.Join(
		s.kasPool.Close(),       // first close the client
		s.inMemListener.Close(), // then close the listener (if not closed already)
	)
}

func newPrivateAPIServerImpl(log *zap.Logger, auxCtx context.Context, cfg *kascfg.ConfigurationFile, tp trace.TracerProvider,
	mp otelmetric.MeterProvider, p propagation.TextMapPropagator, ssh stats.Handler, jwtSecret []byte, factory modshared.RPCAPIFactory,
	ownPrivateAPIHost string, streamProm grpc.StreamServerInterceptor, unaryProm grpc.UnaryServerInterceptor,
	grpcServerErrorReporter grpctool.ServerErrorReporter) (*grpc.Server, *grpc.Server, error) {
	listenCfg := cfg.PrivateApi.Listen
	credsOpt, err := grpctool.MaybeTLSCreds(listenCfg.CertificateFile, listenCfg.KeyFile)
	if err != nil {
		return nil, nil, err
	}
	if ownPrivateAPIHost == "" && len(credsOpt) > 0 {
		log.Sugar().Infof("%s environment variable is not set. Please set it if you want to "+
			"override the server name used for KAS->KAS TLS communication", envVarOwnPrivateAPIHost)
	}

	jwtAuther := grpctool.NewJWTAuther(jwtSecret, api.JWTKAS, api.JWTKAS, func(ctx context.Context) *zap.Logger {
		return modshared.RPCAPIFromContext(ctx).Log()
	})

	keepaliveOpt, sh := grpctool.MaxConnectionAge2GRPCKeepalive(auxCtx, listenCfg.MaxConnectionAge.AsDuration())
	sharedOpts := []grpc.ServerOption{
		keepaliveOpt,
		grpc.StatsHandler(otelgrpc.NewServerHandler(
			otelgrpc.WithTracerProvider(tp),
			otelgrpc.WithMeterProvider(mp),
			otelgrpc.WithPropagators(p),
			otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
		)),
		grpc.StatsHandler(ssh),
		grpc.StatsHandler(sh),
		grpc.SharedWriteBuffer(true),
		grpc.ChainStreamInterceptor(
			streamProm, // 1. measure all invocations
			modshared.StreamRPCAPIInterceptor(factory),                             // 2. inject RPC API
			jwtAuther.StreamServerInterceptor,                                      // 3. auth and maybe log
			grpc_validator.StreamServerInterceptor(),                               // x. wrap with validator
			grpctool.StreamServerErrorReporterInterceptor(grpcServerErrorReporter), //nolint:contextcheck
		),
		grpc.ChainUnaryInterceptor(
			unaryProm, // 1. measure all invocations
			modshared.UnaryRPCAPIInterceptor(factory), // 2. inject RPC API
			jwtAuther.UnaryServerInterceptor,          // 3. auth and maybe log
			grpc_validator.UnaryServerInterceptor(),   // x. wrap with validator
			grpctool.UnaryServerErrorReporterInterceptor(grpcServerErrorReporter),
		),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             20 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.ForceServerCodec(grpctool.RawCodecWithProtoFallback{}),
	}
	server := grpc.NewServer(append(credsOpt, sharedOpts...)...)
	inMemServer := grpc.NewServer(sharedOpts...)
	return server, inMemServer, nil
}

func newKASPool(log *zap.Logger, errRep errz.ErrReporter, tp trace.TracerProvider, mp otelmetric.MeterProvider,
	p propagation.TextMapPropagator, csh stats.Handler, jwtSecret []byte, ownPrivateAPIURL, ownPrivateAPIHost, caCertificateFile string,
	dialer func(context.Context, string) (net.Conn, error),
	streamClientProm grpc.StreamClientInterceptor, unaryClientProm grpc.UnaryClientInterceptor) (grpctool.PoolInterface, error) {

	sharedPoolOpts := []grpc.DialOption{
		grpc.WithSharedWriteBuffer(true),
		// Default gRPC parameters are good, no need to change them at the moment.
		// Specify them explicitly for discoverability.
		// See https://github.com/grpc/grpc/blob/master/doc/connection-backoff.md.
		grpc.WithConnectParams(grpc.ConnectParams{
			Backoff:           backoff.DefaultConfig,
			MinConnectTimeout: 20 * time.Second, // matches the default gRPC value.
		}),
		grpc.WithStatsHandler(otelgrpc.NewClientHandler(
			otelgrpc.WithTracerProvider(tp),
			otelgrpc.WithMeterProvider(mp),
			otelgrpc.WithPropagators(p),
			otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
		)),
		grpc.WithStatsHandler(csh),
		grpc.WithUserAgent(kasServerName()),
		grpc.WithKeepaliveParams(keepalive.ClientParameters{
			Time:                55 * time.Second,
			PermitWithoutStream: true,
		}),
		grpc.WithPerRPCCredentials(&grpctool.JWTCredentials{
			Secret:        jwtSecret,
			Audience:      api.JWTKAS,
			Issuer:        api.JWTKAS,
			SigningMethod: jwt.SigningMethodHS256,
			Insecure:      true, // We may or may not have TLS setup, so always say creds don't need TLS.
		}),
		grpc.WithChainStreamInterceptor(
			streamClientProm,
			grpctool.StreamClientValidatingInterceptor,
		),
		grpc.WithChainUnaryInterceptor(
			unaryClientProm,
			grpctool.UnaryClientValidatingInterceptor,
		),
	}

	// Construct in-memory connection to private API gRPC server
	inMemConn, err := grpc.NewClient("passthrough:private-server",
		append([]grpc.DialOption{
			grpc.WithContextDialer(dialer),
			grpc.WithTransportCredentials(insecure.NewCredentials()),
		}, sharedPoolOpts...)...,
	)
	if err != nil {
		return nil, err
	}
	tlsCreds, err := tlstool.DefaultClientTLSConfigWithCACert(caCertificateFile)
	if err != nil {
		return nil, err
	}
	tlsCreds.ServerName = ownPrivateAPIHost
	kasPool := grpctool.NewPool(log, errRep, credentials.NewTLS(tlsCreds), sharedPoolOpts...)
	return grpctool.NewPoolSelf(kasPool, ownPrivateAPIURL, inMemConn), nil
}

func constructOwnURL(interfaceAddrs func() ([]net.Addr, error),
	ownURL, ownCIDR, ownScheme, ownPort, listenNetwork, listenAddress string) (string, error) {

	if ownURL != "" {
		if ownCIDR != "" {
			return "", fmt.Errorf("either %s or %s should be specified, not both", envVarOwnPrivateAPIURL, envVarOwnPrivateAPICIDR)
		}
		return ownURL, nil
	}

	// Determine port. 0 means not set
	port, err := detectOwnPort(ownPort)
	if err != nil {
		return "", err
	}

	// Determine scheme
	scheme, err := detectOwnScheme(ownScheme)
	if err != nil {
		return "", err
	}

	if ownCIDR != "" {
		return detectURLByCIDR(interfaceAddrs, ownCIDR, scheme, port, listenNetwork, listenAddress)
	}

	return detectURLFromListenAddress(scheme, listenNetwork, listenAddress)
}

func detectOwnScheme(ownScheme string) (string, error) {
	switch ownScheme {
	case "grpc", "grpcs":
		return ownScheme, nil
	case "":
		return "grpc", nil
	default:
		return "", fmt.Errorf("%s environment variable should be either grpc or grpcs, got: %s", envVarOwnPrivateAPIScheme, ownScheme)
	}
}

func detectOwnPort(ownPort string) (uint16, error) {
	if ownPort == "" {
		return 0, nil
	}
	port, err := strconv.ParseUint(ownPort, 10, 16)
	if err != nil {
		return 0, fmt.Errorf("error parsing %s environment variable: %w", envVarOwnPrivateAPIPort, err)
	}
	if port == 0 || port > math.MaxUint16 { // mostly to check for 0, but do a full range check for completeness.
		return 0, fmt.Errorf("invalid port in %s environment variable: %d", envVarOwnPrivateAPIPort, port)
	}
	return uint16(port), nil
}

func detectURLByCIDR(interfaceAddrs func() ([]net.Addr, error),
	ownCIDR, scheme string, port uint16, listenNetwork, listenAddress string) (string, error) {
	_, ipNet, err := net.ParseCIDR(ownCIDR)
	if err != nil {
		return "", fmt.Errorf("failed to parse %s environment variable: %w", envVarOwnPrivateAPICIDR, err)
	}
	addrs, err := interfaceAddrs()
	if err != nil {
		return "", fmt.Errorf("net.InterfaceAddrs(): %w", err)
	}
	var foundIPs []net.IP
	for _, addr := range addrs {
		addrIP, ok := addr.(*net.IPNet)
		if !ok {
			continue
		}
		if ipNet.Contains(addrIP.IP) {
			foundIPs = append(foundIPs, addrIP.IP)
		}
	}
	var ownIP net.IP
	switch len(foundIPs) {
	case 0:
		return "", fmt.Errorf("no IPs matched CIDR specified in the %s environment variable", envVarOwnPrivateAPICIDR)
	case 1:
		ownIP = foundIPs[0]
	default:
		return "", fmt.Errorf("multiple IPs matched CIDR specified in the %s environment variable: %s", envVarOwnPrivateAPICIDR, foundIPs)
	}
	var portStr string
	if port == 0 { // not specified, use port from listener
		switch listenNetwork {
		case "tcp", "tcp4", "tcp6": // assume listenAddress is a ip:port or name:port
			_, listenPort, err := net.SplitHostPort(listenAddress)
			if err != nil {
				return "", fmt.Errorf("listener address: %w", err)
			}
			portStr = listenPort
		//case "unix": We don't handle unix scheme here because we got OWN_PRIVATE_API_CIDR and hence presumably
		// user wants to use network, not unix socket.
		default:
			return "", fmt.Errorf("cannot determine port for own URL. Specify %s", envVarOwnPrivateAPIPort)
		}
	} else {
		portStr = strconv.FormatInt(int64(port), 10)
	}
	return scheme + "://" + net.JoinHostPort(ownIP.String(), portStr), nil
}

func detectURLFromListenAddress(scheme, listenNetwork, listenAddress string) (string, error) {
	switch listenNetwork {
	case "tcp", "tcp4", "tcp6": // assume listenAddress is a ip:port or name:port
		listenHost, _, err := net.SplitHostPort(listenAddress)
		if err != nil {
			return "", fmt.Errorf("listener address: %w", err)
		}
		ip := net.ParseIP(listenHost)
		if ip == nil || !ip.IsUnspecified() {
			// not an IP address or not a wildcard ip, use as is.
			return scheme + "://" + listenAddress, nil
		}
		// Which IP will be used for listening in case of a wildcard listen address depends on many things.
		// See https://github.com/golang/go/blob/go1.21.1/src/net/ipsock_posix.go#L73-L111.
		// Just report an error.
		return "", fmt.Errorf("couldn't determine own URL. Please set %s or %s environment variable", envVarOwnPrivateAPIURL, envVarOwnPrivateAPICIDR)
	case "unix":
		return "unix://" + listenAddress, nil
	default:
		return "", fmt.Errorf("unsupported network type specified in the listener config: %s", listenNetwork)
	}
}
