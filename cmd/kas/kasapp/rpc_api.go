package kasapp

import (
	"context"
	"sync"

	"github.com/getsentry/sentry-go"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
)

type serverRPCAPI struct {
	modshared.RPCAPIStub
	sentryHubRoot *sentry.Hub

	service string
	method  string
	traceID trace.TraceID

	agentInfoResolver modserver.AgentInfoResolver

	sentryHubOnce sync.Once
	sentryHub     SentryHub
	transaction   string
}

func (a *serverRPCAPI) HandleProcessingError(log *zap.Logger, agentID int64, msg string, err error) {
	handleProcessingError(a.StreamCtx, a.hub, log, agentID, msg, err, a.agentInfoResolver)
}

func (a *serverRPCAPI) HandleIOError(log *zap.Logger, msg string, err error) error {
	// The problem is almost certainly with the client's connection.
	// Still log it on Debug.
	log.Debug(msg, logz.Error(err))
	return grpctool.HandleIOError(msg, err)
}

func (a *serverRPCAPI) hub() (SentryHub, string) {
	a.sentryHubOnce.Do(a.hubOnce)
	return a.sentryHub, a.transaction
}

func (a *serverRPCAPI) hubOnce() {
	hub := a.sentryHubRoot.Clone()
	scope := hub.Scope()
	scope.SetTag(modserver.GRPCServiceSentryField, a.service)
	scope.SetTag(modserver.GRPCMethodSentryField, a.method)
	a.transaction = a.service + "::" + a.method                            // Like in Gitaly
	scope.SetFingerprint([]string{"{{ default }}", "grpc", a.transaction}) // use Sentry's default error hash but also split by gRPC transaction
	if a.traceID.IsValid() {
		scope.SetTag(modserver.SentryFieldTraceID, a.traceID.String())
	}
	a.sentryHub = hub
}

type serverRPCAPIFactory struct {
	log               *zap.Logger
	sentryHub         *sentry.Hub
	agentInfoResolver modserver.AgentInfoResolver
}

func (f *serverRPCAPIFactory) New(ctx context.Context, fullMethodName string) modshared.RPCAPI {
	service, method := grpctool.SplitGRPCMethod(fullMethodName)
	traceID := trace.SpanContextFromContext(ctx).TraceID()
	return &serverRPCAPI{
		RPCAPIStub: modshared.RPCAPIStub{
			Logger: f.log.With(
				logz.TraceID(traceID),
				logz.GRPCService(service),
				logz.GRPCMethod(method),
			),
			StreamCtx: ctx,
		},
		sentryHubRoot:     f.sentryHub,
		service:           service,
		method:            method,
		traceID:           traceID,
		agentInfoResolver: f.agentInfoResolver,
	}
}
