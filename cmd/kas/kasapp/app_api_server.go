package kasapp

import (
	"context"
	"fmt"
	"net"
	"time"

	"github.com/ash2k/stager"
	grpc_validator "github.com/grpc-ecosystem/go-grpc-middleware/v2/interceptors/validator"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/api"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/observability"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/ioz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/nettool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/pkg/kascfg"
	"go.opentelemetry.io/contrib/instrumentation/google.golang.org/grpc/otelgrpc"
	otelmetric "go.opentelemetry.io/otel/metric"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/keepalive"
	"google.golang.org/grpc/stats"
)

type apiServer struct {
	log       *zap.Logger
	listenCfg *kascfg.ListenApiCF
	server    *grpc.Server
	auxCancel context.CancelFunc
	ready     func()
}

func newAPIServer(log *zap.Logger, cfg *kascfg.ConfigurationFile, tp trace.TracerProvider, mp otelmetric.MeterProvider,
	p propagation.TextMapPropagator, ssh stats.Handler, factory modshared.RPCAPIFactory,
	probeRegistry *observability.ProbeRegistry, streamProm grpc.StreamServerInterceptor, unaryProm grpc.UnaryServerInterceptor,
	grpcServerErrorReporter grpctool.ServerErrorReporter) (*apiServer, error) {
	listenCfg := cfg.Api.Listen
	jwtSecret, err := ioz.LoadBase64Secret(listenCfg.AuthenticationSecretFile)
	if err != nil {
		return nil, fmt.Errorf("auth secret file: %w", err)
	}
	credsOpt, err := grpctool.MaybeTLSCreds(listenCfg.CertificateFile, listenCfg.KeyFile)
	if err != nil {
		return nil, err
	}

	jwtAuther := grpctool.NewJWTAuther(jwtSecret, "", api.JWTKAS, func(ctx context.Context) *zap.Logger {
		return modshared.RPCAPIFromContext(ctx).Log()
	})

	auxCtx, auxCancel := context.WithCancel(context.Background())
	keepaliveOpt, sh := grpctool.MaxConnectionAge2GRPCKeepalive(auxCtx, listenCfg.MaxConnectionAge.AsDuration())
	serverOpts := []grpc.ServerOption{
		grpc.StatsHandler(otelgrpc.NewServerHandler(
			otelgrpc.WithTracerProvider(tp),
			otelgrpc.WithMeterProvider(mp),
			otelgrpc.WithPropagators(p),
			otelgrpc.WithMessageEvents(otelgrpc.ReceivedEvents, otelgrpc.SentEvents),
		)),
		grpc.StatsHandler(ssh),
		grpc.StatsHandler(sh),
		grpc.SharedWriteBuffer(true),
		grpc.ChainStreamInterceptor(
			streamProm, // 1. measure all invocations
			modshared.StreamRPCAPIInterceptor(factory), // 2. inject RPC API
			jwtAuther.StreamServerInterceptor,          // 3. auth and maybe log
			grpc_validator.StreamServerInterceptor(),   // x. wrap with validator
			grpctool.StreamServerErrorReporterInterceptor(grpcServerErrorReporter),
		),
		grpc.ChainUnaryInterceptor(
			unaryProm, // 1. measure all invocations
			modshared.UnaryRPCAPIInterceptor(factory), // 2. inject RPC API
			jwtAuther.UnaryServerInterceptor,          // 3. auth and maybe log
			grpc_validator.UnaryServerInterceptor(),   // x. wrap with validator
			grpctool.UnaryServerErrorReporterInterceptor(grpcServerErrorReporter),
		),
		grpc.KeepaliveEnforcementPolicy(keepalive.EnforcementPolicy{
			MinTime:             20 * time.Second,
			PermitWithoutStream: true,
		}),
		keepaliveOpt,
	}

	serverOpts = append(serverOpts, credsOpt...)

	return &apiServer{
		log:       log,
		listenCfg: listenCfg,
		server:    grpc.NewServer(serverOpts...),
		auxCancel: auxCancel,
		ready:     probeRegistry.RegisterReadinessToggle("apiServer"),
	}, nil
}

func (s *apiServer) Start(stage stager.Stage) {
	grpctool.StartServer(stage, s.server, func() (net.Listener, error) {
		lis, err := nettool.ListenWithOSTCPKeepAlive(*s.listenCfg.Network, s.listenCfg.Address)
		if err != nil {
			return nil, err
		}
		addr := lis.Addr()
		s.log.Info("API endpoint is up",
			logz.NetNetworkFromAddr(addr),
			logz.NetAddressFromAddr(addr),
		)
		s.ready()
		return lis, nil
	}, func() {
		time.Sleep(s.listenCfg.ListenGracePeriod.AsDuration())
		// We first want gRPC server to send GOAWAY and only then return from the RPC handlers.
		// So we delay signaling the handlers and registry.
		// See https://github.com/grpc/grpc-go/issues/6830 for more background.
		// Start a goroutine in a second and signal running RPC handlers to stop.
		time.AfterFunc(time.Second, s.auxCancel)
	})
}
