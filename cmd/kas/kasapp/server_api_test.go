package kasapp

import (
	"context"
	"errors"
	"strconv"
	"testing"

	"github.com/getsentry/sentry-go"
	"github.com/google/go-cmp/cmp"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/errz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/prototool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/testhelpers"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/rpc"
	"go.opentelemetry.io/otel/trace"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap"
	"go.uber.org/zap/zaptest"
	"google.golang.org/protobuf/proto"
	"google.golang.org/protobuf/testing/protocmp"
)

var (
	_ modserver.API = (*serverAPI)(nil)
)

func TestHandleProcessingError_UserError(t *testing.T) {
	ctx, log, _, apiObj, _, _ := setupAPI(t)
	err := errz.NewUserError("boom")
	apiObj.HandleProcessingError(ctx, log, testhelpers.AgentID, "Bla", err)
}

func TestHandleProcessingError_NonUserError_AgentID(t *testing.T) {
	ctx, log, hub, apiObj, traceID, agentInfoResolver := setupAPI(t)
	err := errors.New("boom")
	hub.EXPECT().
		CaptureEvent(gomock.Any()).
		Do(func(event *sentry.Event) *sentry.EventID {
			assert.Equal(t, traceID.String(), event.Tags[modserver.SentryFieldTraceID])
			assert.Equal(t, strconv.FormatInt(testhelpers.AgentID, 10), event.User.ID)
			assert.Equal(t, sentry.LevelError, event.Level)
			assert.Equal(t, "*errors.errorString", event.Exception[0].Type)
			assert.Equal(t, "Bla: boom", event.Exception[0].Value)
			return nil
		})

	agentInfoResolver.EXPECT().Get(gomock.Any(), testhelpers.AgentID)
	apiObj.HandleProcessingError(ctx, log, testhelpers.AgentID, "Bla", err)
}

func TestHandleProcessingError_NonUserError_AgentID_WithExtraData(t *testing.T) {
	ctx, log, hub, apiObj, traceID, agentInfoResolver := setupAPI(t)
	err := errors.New("boom")
	hub.EXPECT().
		CaptureEvent(gomock.Any()).
		Do(func(event *sentry.Event) *sentry.EventID {
			assert.Equal(t, traceID.String(), event.Tags[modserver.SentryFieldTraceID])
			assert.Equal(t, strconv.FormatInt(testhelpers.AgentID, 10), event.User.ID)
			assert.Equal(t, sentry.LevelError, event.Level)
			assert.Equal(t, "*errors.errorString", event.Exception[0].Type)
			assert.Equal(t, "Bla: boom", event.Exception[0].Value)
			assert.Equal(t, map[string]interface{}{"foo": "bar", "any": 2}, event.Extra)
			return nil
		})

	agentInfoResolver.EXPECT().Get(gomock.Any(), testhelpers.AgentID).Return(map[string]interface{}{"foo": "bar", "any": 2}, nil)
	apiObj.HandleProcessingError(ctx, log, testhelpers.AgentID, "Bla", err)
}

func TestHandleProcessingError_NonUserError_NoAgentId_NoTraceID(t *testing.T) {
	_, log, hub, apiObj, _, _ := setupAPI(t)
	err := errors.New("boom")
	hub.EXPECT().
		CaptureEvent(gomock.Any()).
		Do(func(event *sentry.Event) *sentry.EventID {
			assert.NotContains(t, event.Tags, modserver.SentryFieldTraceID)
			assert.Empty(t, event.User.ID)
			assert.Equal(t, sentry.LevelError, event.Level)
			assert.Equal(t, "*errors.errorString", event.Exception[0].Type)
			assert.Equal(t, "Bla: boom", event.Exception[0].Value)
			return nil
		})
	apiObj.HandleProcessingError(context.Background(), log, modshared.NoAgentID, "Bla", err)
}

func setupAPI(t *testing.T) (context.Context, *zap.Logger, *MockSentryHub, *serverAPI, trace.TraceID, *mock_modserver.MockAgentInfoResolver) {
	log := zaptest.NewLogger(t)
	ctrl := gomock.NewController(t)
	hub := NewMockSentryHub(ctrl)
	agentInfoResolver := mock_modserver.NewMockAgentInfoResolver(ctrl)
	ctx, traceID := testhelpers.CtxWithSpanContext(t)
	apiObj := newServerAPI(log, hub, nil, agentInfoResolver)
	return ctx, log, hub, apiObj, traceID, agentInfoResolver
}

func TestRemoveRandomPort(t *testing.T) {
	tests := []struct {
		input    string
		expected string
	}{
		{
			input:    "",
			expected: "",
		},
		{
			input:    "bla",
			expected: "bla",
		},
		{
			input:    "read tcp 10.222.67.20:40272->10.216.1.45:11443: read: connection reset by peer",
			expected: "read tcp 10.222.67.20:x->10.216.1.45:11443: read: connection reset by peer",
		},
		{
			input:    "some error with ip and port 10.222.67.20:40272: bla",
			expected: "some error with ip and port 10.222.67.20:40272: bla",
		},
	}
	for _, tc := range tests {
		t.Run(tc.input, func(t *testing.T) {
			actual := removeRandomPort(tc.input)
			assert.Equal(t, tc.expected, actual)
		})
	}
}

func TestRedisMarshalAndUnmarshal(t *testing.T) {
	mIn := &rpc.GatewayResponse_Header{
		Meta: map[string]*prototool.Values{
			"key": {Value: []string{"1", "2"}},
		},
	}
	msg, err := redisProtoMarshal(mIn)
	require.NoError(t, err)
	mOut, err := redisProtoUnmarshal(string(msg))
	require.NoError(t, err)

	assert.Empty(t, cmp.Diff(mIn, mOut, protocmp.Transform()))
}

func TestRedisUnmarshalErr(t *testing.T) {
	_, err := redisProtoUnmarshal("")
	assert.ErrorIs(t, err, proto.Error)
}
