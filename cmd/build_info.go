package cmd

import (
	"fmt"
	"time"
)

var (
	Version            = "v0.0.0"
	Commit             = "00000000"
	BuildTimeFormatted = "1970-01-01T00:00:00+00:00"
	BuildTime          time.Time
)

func init() {
	bt, err := time.Parse(time.RFC3339, BuildTimeFormatted)
	if err != nil {
		panic(fmt.Errorf("failed to parse %q into RFC3339 compliant time object, because: %w. Fix the build process.", BuildTimeFormatted, err))
	}
	BuildTime = bt
}
