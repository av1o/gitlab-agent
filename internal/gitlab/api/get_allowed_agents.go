package api

import (
	"context"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/gitlab"
)

const (
	AllowedAgentsAPIPath = "/api/v4/job/allowed_agents"
)

func GetAllowedAgentsForJob(ctx context.Context, client gitlab.ClientInterface, jobToken string, opts ...gitlab.DoOption) (*AllowedAgentsForJob, error) {
	aa := &AllowedAgentsForJob{}
	err := client.Do(ctx,
		joinOpts(opts,
			gitlab.WithPath(AllowedAgentsAPIPath),
			gitlab.WithJobToken(jobToken),
			gitlab.WithResponseHandler(gitlab.ProtoJSONResponseHandler(aa)),
		)...,
	)
	if err != nil {
		return nil, err
	}
	return aa, nil
}
