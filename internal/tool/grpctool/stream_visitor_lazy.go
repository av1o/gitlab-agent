package grpctool

import (
	"sync"

	"google.golang.org/protobuf/proto"
)

func NewLazyStreamVisitor(streamMessage proto.Message) func() *StreamVisitor {
	return sync.OnceValue(func() *StreamVisitor {
		sv, err := NewStreamVisitor(streamMessage)
		if err != nil {
			panic(err) // this will never panic as long as the proto file is correct
		}
		return sv
	})
}
