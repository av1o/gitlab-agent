// Code generated by MockGen. DO NOT EDIT.
// Source: gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_stdlib (interfaces: ResponseWriterFlusher)
//
// Generated by this command:
//
//	mockgen -typed -destination net_http_custom.go -package mock_stdlib gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_stdlib ResponseWriterFlusher
//

// Package mock_stdlib is a generated GoMock package.
package mock_stdlib

import (
	bufio "bufio"
	net "net"
	http "net/http"
	reflect "reflect"

	gomock "go.uber.org/mock/gomock"
)

// MockResponseWriterFlusher is a mock of ResponseWriterFlusher interface.
type MockResponseWriterFlusher struct {
	ctrl     *gomock.Controller
	recorder *MockResponseWriterFlusherMockRecorder
}

// MockResponseWriterFlusherMockRecorder is the mock recorder for MockResponseWriterFlusher.
type MockResponseWriterFlusherMockRecorder struct {
	mock *MockResponseWriterFlusher
}

// NewMockResponseWriterFlusher creates a new mock instance.
func NewMockResponseWriterFlusher(ctrl *gomock.Controller) *MockResponseWriterFlusher {
	mock := &MockResponseWriterFlusher{ctrl: ctrl}
	mock.recorder = &MockResponseWriterFlusherMockRecorder{mock}
	return mock
}

// EXPECT returns an object that allows the caller to indicate expected use.
func (m *MockResponseWriterFlusher) EXPECT() *MockResponseWriterFlusherMockRecorder {
	return m.recorder
}

// Flush mocks base method.
func (m *MockResponseWriterFlusher) Flush() {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "Flush")
}

// Flush indicates an expected call of Flush.
func (mr *MockResponseWriterFlusherMockRecorder) Flush() *MockResponseWriterFlusherFlushCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Flush", reflect.TypeOf((*MockResponseWriterFlusher)(nil).Flush))
	return &MockResponseWriterFlusherFlushCall{Call: call}
}

// MockResponseWriterFlusherFlushCall wrap *gomock.Call
type MockResponseWriterFlusherFlushCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockResponseWriterFlusherFlushCall) Return() *MockResponseWriterFlusherFlushCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockResponseWriterFlusherFlushCall) Do(f func()) *MockResponseWriterFlusherFlushCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockResponseWriterFlusherFlushCall) DoAndReturn(f func()) *MockResponseWriterFlusherFlushCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Header mocks base method.
func (m *MockResponseWriterFlusher) Header() http.Header {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Header")
	ret0, _ := ret[0].(http.Header)
	return ret0
}

// Header indicates an expected call of Header.
func (mr *MockResponseWriterFlusherMockRecorder) Header() *MockResponseWriterFlusherHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Header", reflect.TypeOf((*MockResponseWriterFlusher)(nil).Header))
	return &MockResponseWriterFlusherHeaderCall{Call: call}
}

// MockResponseWriterFlusherHeaderCall wrap *gomock.Call
type MockResponseWriterFlusherHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockResponseWriterFlusherHeaderCall) Return(arg0 http.Header) *MockResponseWriterFlusherHeaderCall {
	c.Call = c.Call.Return(arg0)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockResponseWriterFlusherHeaderCall) Do(f func() http.Header) *MockResponseWriterFlusherHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockResponseWriterFlusherHeaderCall) DoAndReturn(f func() http.Header) *MockResponseWriterFlusherHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Hijack mocks base method.
func (m *MockResponseWriterFlusher) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Hijack")
	ret0, _ := ret[0].(net.Conn)
	ret1, _ := ret[1].(*bufio.ReadWriter)
	ret2, _ := ret[2].(error)
	return ret0, ret1, ret2
}

// Hijack indicates an expected call of Hijack.
func (mr *MockResponseWriterFlusherMockRecorder) Hijack() *MockResponseWriterFlusherHijackCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Hijack", reflect.TypeOf((*MockResponseWriterFlusher)(nil).Hijack))
	return &MockResponseWriterFlusherHijackCall{Call: call}
}

// MockResponseWriterFlusherHijackCall wrap *gomock.Call
type MockResponseWriterFlusherHijackCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockResponseWriterFlusherHijackCall) Return(arg0 net.Conn, arg1 *bufio.ReadWriter, arg2 error) *MockResponseWriterFlusherHijackCall {
	c.Call = c.Call.Return(arg0, arg1, arg2)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockResponseWriterFlusherHijackCall) Do(f func() (net.Conn, *bufio.ReadWriter, error)) *MockResponseWriterFlusherHijackCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockResponseWriterFlusherHijackCall) DoAndReturn(f func() (net.Conn, *bufio.ReadWriter, error)) *MockResponseWriterFlusherHijackCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// Write mocks base method.
func (m *MockResponseWriterFlusher) Write(arg0 []byte) (int, error) {
	m.ctrl.T.Helper()
	ret := m.ctrl.Call(m, "Write", arg0)
	ret0, _ := ret[0].(int)
	ret1, _ := ret[1].(error)
	return ret0, ret1
}

// Write indicates an expected call of Write.
func (mr *MockResponseWriterFlusherMockRecorder) Write(arg0 any) *MockResponseWriterFlusherWriteCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "Write", reflect.TypeOf((*MockResponseWriterFlusher)(nil).Write), arg0)
	return &MockResponseWriterFlusherWriteCall{Call: call}
}

// MockResponseWriterFlusherWriteCall wrap *gomock.Call
type MockResponseWriterFlusherWriteCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockResponseWriterFlusherWriteCall) Return(arg0 int, arg1 error) *MockResponseWriterFlusherWriteCall {
	c.Call = c.Call.Return(arg0, arg1)
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockResponseWriterFlusherWriteCall) Do(f func([]byte) (int, error)) *MockResponseWriterFlusherWriteCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockResponseWriterFlusherWriteCall) DoAndReturn(f func([]byte) (int, error)) *MockResponseWriterFlusherWriteCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}

// WriteHeader mocks base method.
func (m *MockResponseWriterFlusher) WriteHeader(arg0 int) {
	m.ctrl.T.Helper()
	m.ctrl.Call(m, "WriteHeader", arg0)
}

// WriteHeader indicates an expected call of WriteHeader.
func (mr *MockResponseWriterFlusherMockRecorder) WriteHeader(arg0 any) *MockResponseWriterFlusherWriteHeaderCall {
	mr.mock.ctrl.T.Helper()
	call := mr.mock.ctrl.RecordCallWithMethodType(mr.mock, "WriteHeader", reflect.TypeOf((*MockResponseWriterFlusher)(nil).WriteHeader), arg0)
	return &MockResponseWriterFlusherWriteHeaderCall{Call: call}
}

// MockResponseWriterFlusherWriteHeaderCall wrap *gomock.Call
type MockResponseWriterFlusherWriteHeaderCall struct {
	*gomock.Call
}

// Return rewrite *gomock.Call.Return
func (c *MockResponseWriterFlusherWriteHeaderCall) Return() *MockResponseWriterFlusherWriteHeaderCall {
	c.Call = c.Call.Return()
	return c
}

// Do rewrite *gomock.Call.Do
func (c *MockResponseWriterFlusherWriteHeaderCall) Do(f func(int)) *MockResponseWriterFlusherWriteHeaderCall {
	c.Call = c.Call.Do(f)
	return c
}

// DoAndReturn rewrite *gomock.Call.DoAndReturn
func (c *MockResponseWriterFlusherWriteHeaderCall) DoAndReturn(f func(int)) *MockResponseWriterFlusherWriteHeaderCall {
	c.Call = c.Call.DoAndReturn(f)
	return c
}
