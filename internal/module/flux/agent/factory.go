package agent

import (
	"context"
	"net/http"
	"time"

	notificationv1 "github.com/fluxcd/notification-controller/api/v1"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/flux/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"go.uber.org/zap"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/transport"
)

const (
	// resyncDuration defines the duration for the shared informer cache resync interval.
	resyncDuration = 10 * time.Minute

	reconcileProjectsInitBackoff   = 10 * time.Second
	reconcileProjectsMaxBackoff    = 5 * time.Minute
	reconcileProjectsResetDuration = 10 * time.Minute
	reconcileProjectsBackoffFactor = 2.0
	reconcileProjectsJitter        = 1.0
)

var (
	requiredFluxCRDs = [...]schema.GroupVersionResource{
		sourcev1.GroupVersion.WithResource("gitrepositories"),
		notificationv1.GroupVersion.WithResource("receivers"),
	}
)

type Factory struct{}

func (f *Factory) IsProducingLeaderModules() bool {
	return true
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	restConfig, err := config.K8sUtilFactory.ToRESTConfig()
	if err != nil {
		return nil, err
	}

	dynamicClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	if !isFluxInstalled(context.Background(), config.Log, config.AgentMeta.GetPodNamespace(), dynamicClient) {
		config.Log.Info("Flux is not installed, skipping module. A restart is required for this to be checked again")
		return nil, nil
	}

	clientset, err := kubernetes.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	receiverClient := dynamicClient.Resource(notificationv1.GroupVersion.WithResource("receivers"))

	kubeAPIURL, _, err := defaultServerURLFor(restConfig)
	if err != nil {
		return nil, err
	}
	transportCfg, err := restConfig.TransportConfig()
	if err != nil {
		return nil, err
	}
	kubeAPIRoundTripper, err := transport.New(transportCfg)
	if err != nil {
		return nil, err
	}

	return &module{
		log: config.Log,
		runner: &fluxReconciliationRunner{
			informersFactory: func() (informers.GenericInformer, informers.GenericInformer, cache.Indexer) {
				informerFactory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(dynamicClient, resyncDuration, config.AgentMeta.GetPodNamespace(), nil)
				gitRepositoryInformer := informerFactory.ForResource(sourcev1.GroupVersion.WithResource("gitrepositories"))
				receiverInformer := informerFactory.ForResource(notificationv1.GroupVersion.WithResource("receivers"))
				receiverIndexer := receiverInformer.Informer().GetIndexer()
				return gitRepositoryInformer, receiverInformer, receiverIndexer
			},
			clientFactory: func(ctx context.Context, cfgURL string, receiverIndexer cache.Indexer) (*client, error) {
				agentID, err := config.API.GetAgentID(ctx)
				if err != nil {
					return nil, err
				}

				rt, err := newGitRepositoryReconcileTrigger(cfgURL, kubeAPIURL, kubeAPIRoundTripper, http.DefaultTransport)
				if err != nil {
					return nil, err
				}

				return newClient(
					config.Log,
					config.API,
					agentID,
					rpc.NewGitLabFluxClient(config.KASConn),
					retry.NewPollConfigFactory(0, retry.NewExponentialBackoffFactory(
						reconcileProjectsInitBackoff, reconcileProjectsMaxBackoff, reconcileProjectsResetDuration, reconcileProjectsBackoffFactor, reconcileProjectsJitter),
					),
					receiverIndexer,
					rt,
				)
			},
			controllerFactory: func(ctx context.Context, gitRepositoryInformer informers.GenericInformer, receiverInformer informers.GenericInformer, projectReconciler projectReconciler) (controller, error) {
				agentID, err := config.API.GetAgentID(ctx)
				if err != nil {
					return nil, err
				}
				gitLabExternalURL, err := config.API.GetGitLabExternalURL(ctx)
				if err != nil {
					return nil, err
				}

				return newGitRepositoryController(ctx, config.Log, config.API, agentID, gitLabExternalURL, gitRepositoryInformer, receiverInformer, projectReconciler, receiverClient, clientset.CoreV1())
			},
		},
	}, nil
}

func (f *Factory) Name() string {
	return flux.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}

func isFluxInstalled(ctx context.Context, log *zap.Logger, namespace string, client dynamic.Interface) bool {
	for _, crd := range requiredFluxCRDs {
		_, err := checkCRDExistsAndEstablished(ctx, client, namespace, crd)
		if err != nil {
			log.Error("failed to list flux resource", logz.K8sGroup(crd.Group), logz.Error(err))
			return false
		}
	}
	return true
}

func checkCRDExistsAndEstablished(ctx context.Context, client dynamic.Interface, namespace string, crd schema.GroupVersionResource) (bool, error) {
	_, err := client.Resource(crd).Namespace(namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return false, err
	}
	return true, nil
}
