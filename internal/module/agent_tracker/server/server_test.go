package server

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/agent_tracker/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_agent_tracker"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/testing/mock_modserver"
	"go.uber.org/mock/gomock"
	"go.uber.org/zap/zaptest"
)

var (
	_ modserver.Factory = (*Factory)(nil)
)

func TestServer_GetConnectedAgentsByProjectIDs(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	projectID1 := int64(123)
	projectID2 := int64(456)
	req := &rpc.GetConnectedAgentsByProjectIDsRequest{
		ProjectIds: []int64{projectID1, projectID2},
	}

	mockRPCAPI.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	mockTracker.EXPECT().
		GetConnectionsByProjectID(ctx, projectID1, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      123123,
				ProjectId:    projectID1,
			})
			require.NoError(t, err)
			return err
		})
	mockTracker.EXPECT().
		GetConnectionsByProjectID(ctx, projectID2, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      456456,
				ProjectId:    projectID2,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByProjectIDs(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, 123123, resp.Agents[0].AgentId)
	assert.EqualValues(t, 456456, resp.Agents[1].AgentId)
}

func TestServer_GetConnectedAgentsByAgentIDs(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	agentID1 := int64(123)
	agentID2 := int64(456)
	req := &rpc.GetConnectedAgentsByAgentIDsRequest{
		AgentIds: []int64{agentID1, agentID2},
	}

	mockRPCAPI.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	mockTracker.EXPECT().
		GetConnectionsByAgentID(ctx, agentID1, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 123123123,
				AgentId:      agentID1,
				ProjectId:    123123,
			})
			require.NoError(t, err)
			return err
		})
	mockTracker.EXPECT().
		GetConnectionsByAgentID(ctx, agentID2, gomock.Any()).
		Do(func(ctx context.Context, projectID int64, cb agent_tracker.ConnectedAgentInfoCallback) error {
			_, err := cb(&agent_tracker.ConnectedAgentInfo{
				ConnectionId: 456456456,
				AgentId:      agentID2,
				ProjectId:    456456,
			})
			require.NoError(t, err)
			return err
		})

	resp, err := s.GetConnectedAgentsByAgentIDs(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.Agents, 2)
	assert.EqualValues(t, agentID1, resp.Agents[0].AgentId)
	assert.EqualValues(t, agentID2, resp.Agents[1].AgentId)
}

func TestServer_CountAgentsByAgentVersions(t *testing.T) {
	mockRPCAPI, mockTracker, s, ctx := setupServer(t)

	req := &rpc.CountAgentsByAgentVersionsRequest{}

	mockRPCAPI.EXPECT().
		Log().
		Return(zaptest.NewLogger(t))

	mockTracker.EXPECT().
		CountAgentsByAgentVersions(ctx).
		DoAndReturn(func(ctx context.Context) (map[string]int64, error) {
			counts := map[string]int64{
				"16.8.0": 111,
				"16.9.0": 222,
			}
			return counts, nil
		})

	resp, err := s.CountAgentsByAgentVersions(ctx, req)
	require.NoError(t, err)
	assert.Len(t, resp.AgentVersions, 2)
	assert.EqualValues(t, 111, resp.AgentVersions["16.8.0"])
	assert.EqualValues(t, 222, resp.AgentVersions["16.9.0"])
}

func setupServer(t *testing.T) (*mock_modserver.MockAgentRPCAPI, *mock_agent_tracker.MockTracker, *server, context.Context) {
	ctrl := gomock.NewController(t)

	mockRPCAPI := mock_modserver.NewMockAgentRPCAPI(ctrl)
	mockTracker := mock_agent_tracker.NewMockTracker(ctrl)

	s := &server{
		agentQuerier: mockTracker,
	}

	ctx := modshared.InjectRPCAPI(context.Background(), mockRPCAPI)

	return mockRPCAPI, mockTracker, s, ctx
}
