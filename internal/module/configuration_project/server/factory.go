package server

import (
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/configuration_project"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/configuration_project/rpc"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modserver"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
)

type Factory struct {
}

func (f *Factory) New(config *modserver.Config) (modserver.Module, error) {
	rpc.RegisterConfigurationProjectServer(config.APIServer, &server{
		gitaly: config.Gitaly,
	})
	return &modserver.NopModule{
		ModuleName: configuration_project.ModuleName,
	}, nil
}

func (f *Factory) Name() string {
	return configuration_project.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}
