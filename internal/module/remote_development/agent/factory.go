package agent

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modagent"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/remote_development"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/remote_development/agent/k8s"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
)

const (
	interval      = 10 * time.Second
	initBackoff   = 10 * time.Second
	maxBackoff    = time.Minute
	resetDuration = 2 * time.Minute
	backoffFactor = 2.0
	jitter        = 1.0

	agentIDLabelSelector = "agent.gitlab.com/id"
	reReconcileDuration  = 5 * time.Minute
)

var (
	deploymentGVR = schema.GroupVersionResource{
		Group:    "apps",
		Version:  "v1",
		Resource: "deployments",
	}
)

type Factory struct {
}

func (f *Factory) IsProducingLeaderModules() bool {
	return true
}

func (f *Factory) New(config *modagent.Config) (modagent.Module, error) {
	restConfig, err := config.K8sUtilFactory.ToRESTConfig()
	if err != nil {
		return nil, err
	}

	client, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		return nil, err
	}

	k8sClient, err := k8s.New(config.Log, config.AgentMeta.GetPodNamespace(), config.K8sUtilFactory)
	if err != nil {
		return nil, err
	}

	pollFactory := retry.NewPollConfigFactory(interval, retry.NewExponentialBackoffFactory(
		initBackoff,
		maxBackoff,
		resetDuration,
		backoffFactor,
		jitter,
	))

	return &module{
		log: config.Log,
		api: config.API,
		reconcilerFactory: func(ctx context.Context) (remoteDevReconciler, error) {
			agentID, err := config.API.GetAgentID(ctx)
			if err != nil {
				return nil, err
			}

			factory := dynamicinformer.NewFilteredDynamicSharedInformerFactory(client, reReconcileDuration, config.AgentMeta.GetPodNamespace(), func(opts *metav1.ListOptions) {
				opts.LabelSelector = fmt.Sprintf("%s=%d", agentIDLabelSelector, agentID)
			})
			inf, err := newK8sInformer(config.Log, factory.ForResource(deploymentGVR).Informer())
			if err != nil {
				return nil, err
			}

			r := &reconciler{
				log:                config.Log,
				agentID:            agentID,
				api:                config.API,
				pollConfig:         pollFactory,
				stateTracker:       newPersistedStateTracker(),
				terminationTracker: newTerminationTracker(),
				informer:           inf,
				k8sClient:          k8sClient,
				errDetailsTracker:  newErrorDetailsTracker(),
				namespace:          config.AgentMeta.GetPodNamespace(),
			}

			err = r.informer.Start(ctx)
			if err != nil {
				return nil, err
			}

			return r, nil
		},
	}, nil
}

func (f *Factory) Name() string {
	return remote_development.ModuleName
}

func (f *Factory) StartStopPhase() modshared.ModuleStartStopPhase {
	return modshared.ModuleStartBeforeServers
}
