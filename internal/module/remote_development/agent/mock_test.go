package agent

import (
	"context"
)

type MockClient struct {
	NamespaceStore map[string]struct{}
	ApplyRecorder  string
	MockError      error
}

func NewMockClient() *MockClient {
	return &MockClient{
		NamespaceStore: map[string]struct{}{},
	}
}

func (m *MockClient) Apply(_ context.Context, config string) <-chan error {
	m.ApplyRecorder = config

	if m.MockError != nil {
		errorCh := make(chan error)
		go func() {
			defer close(errorCh)
			errorCh <- m.MockError
			m.MockError = nil
		}()

		return errorCh
	}

	return nil
}

func (m *MockClient) Delete(context.Context, string) error {
	return nil
}
