package k8s

import "context"

type Client interface {
	Apply(ctx context.Context, config string) <-chan error
	Delete(ctx context.Context, config string) error
}
