package k8s

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestNewInventoryEntry(t *testing.T) {
	t.Run("full id", func(t *testing.T) {
		e := NewInventoryEntry("gitlab-agent_workspace-5-4-2c7duy_networking.k8s.io_NetworkPolicy")
		assert.EqualValues(t, "gitlab-agent", e.Namespace)
		assert.EqualValues(t, "workspace-5-4-2c7duy", e.Name)
		assert.EqualValues(t, "networking.k8s.io", e.Group)
		assert.EqualValues(t, "NetworkPolicy", e.Kind)
	})
	t.Run("core id", func(t *testing.T) {
		e := NewInventoryEntry("gitlab-agent_workspace-5-4-2c7duy__Service")
		assert.EqualValues(t, "gitlab-agent", e.Namespace)
		assert.EqualValues(t, "workspace-5-4-2c7duy", e.Name)
		assert.EqualValues(t, "", e.Group)
		assert.EqualValues(t, "Service", e.Kind)
	})
}
