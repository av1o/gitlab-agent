package k8s

import (
	"k8s.io/apimachinery/pkg/runtime/schema"
	"strings"
)

type InventoryEntry struct {
	Namespace string
	Name      string
	Group     string
	Kind      string
}

func NewInventoryEntry(s string) InventoryEntry {
	names := strings.Split(s, "_")
	namespace := names[0]
	name := names[1]
	group := names[2]
	kind := names[3]
	return InventoryEntry{
		Namespace: namespace,
		Name:      name,
		Group:     group,
		Kind:      kind,
	}
}

func (e *InventoryEntry) GroupKind() schema.GroupKind {
	return schema.GroupKind{
		Group: e.Group,
		Kind:  e.Kind,
	}
}
