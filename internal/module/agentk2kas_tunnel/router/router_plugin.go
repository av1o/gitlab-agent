package router

import (
	"context"
	"strconv"
	"time"

	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/module/modshared"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/grpctool"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/logz"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tool/retry"
	"gitlab.com/gitlab-org/cluster-integration/gitlab-agent/v17/internal/tunnel/tunserver"
	"go.uber.org/zap"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	_ tunserver.RouterPlugin = (*RouterPlugin)(nil)
)

const (
	// routingAgentIDMetadataKey is used to pass destination agent id in request metadata
	// from the routing kas instance, that is handling the incoming request, to the gateway kas instance,
	// that is forwarding the request to an agentk.
	routingAgentIDMetadataKey = tunserver.RoutingHopPrefix + "routing-agent-id"
)

type RouterPlugin struct {
	KASPool               grpctool.PoolInterface
	GatewayQuerier        tunserver.PollingGatewayURLQuerier
	API                   modshared.API
	TunnelRegistry        *Registry
	OwnPrivateAPIURL      string
	PollConfig            retry.PollConfigFactory
	TryNewGatewayInterval time.Duration
}

func (p *RouterPlugin) GatewayFinder(ctx context.Context, log *zap.Logger, method string) (tunserver.GatewayFinder, *zap.Logger, int64, error) {
	md, _ := metadata.FromOutgoingContext(ctx)
	agentID, err := agentIDFromMeta(md)
	if err != nil {
		return nil, nil, 0, err // returns gRPC status error
	}

	log = log.With(logz.AgentID(agentID))
	gf := tunserver.NewGatewayFinder(
		ctx,
		log,
		p.KASPool,
		p.GatewayQuerier,
		p.API,
		method,
		p.OwnPrivateAPIURL,
		agentID,
		p.PollConfig,
		p.TryNewGatewayInterval,
	)
	return gf, log, agentID, nil
}

func (p *RouterPlugin) FindTunnel(stream grpc.ServerStream, rpcAPI modshared.RPCAPI) (bool, *zap.Logger, tunserver.FindHandle, error) {
	ctx := stream.Context()
	md, _ := metadata.FromIncomingContext(ctx)
	agentID, err := agentIDFromMeta(md)
	if err != nil {
		return false, nil, nil, err
	}
	sts := grpc.ServerTransportStreamFromContext(ctx)
	service, method := grpctool.SplitGRPCMethod(sts.Method())
	log := rpcAPI.Log().With(logz.AgentID(agentID))
	found, handle := p.TunnelRegistry.FindTunnel(ctx, agentID, service, method)
	return found, log, handle, nil
}

func SetRoutingMetadata(md metadata.MD, agentID int64) metadata.MD {
	if md == nil {
		md = metadata.MD{}
	}
	md[routingAgentIDMetadataKey] = []string{strconv.FormatInt(agentID, 10)}
	return md
}

// agentIDFromMeta returns agent ID or an gRPC status error.
func agentIDFromMeta(md metadata.MD) (int64, error) {
	val := md.Get(routingAgentIDMetadataKey)
	if len(val) != 1 {
		return 0, status.Errorf(codes.InvalidArgument, "expecting a single %s, got %d", routingAgentIDMetadataKey, len(val))
	}
	agentID, err := strconv.ParseInt(val[0], 10, 64)
	if err != nil {
		return 0, status.Errorf(codes.InvalidArgument, "invalid %s", routingAgentIDMetadataKey)
	}
	return agentID, nil
}
