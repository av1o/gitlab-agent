package agent_configuration //nolint:stylecheck

const (
	Directory  = ".gitlab/agents"
	FileName   = "config.yaml"
	ModuleName = "agent_configuration"
)
