#!/usr/bin/env bash

# This command is used by bazel as the workspace_status_command
# to implement build stamping with git information.

set -o errexit
set -o nounset
set -o pipefail

# NOTE:
# Building the `gitlab-agent` project, that is KAS and agentk,
# is supported in multiple contexts.
# For official KAS releases happening in CNG and Omnibus, the binaries
# are built from an archive (non-git) source tree.
# Whereas official agentk releases are happening in the canonical gitlab-agent
# repository where it is built from a git source tree.
# Therefore, we use the following logic to determine the release `VERSION`:
# - If git is available `git describe` is used
#   - For revisions that are pointed to by a tag, the tag is used as is.
#     If the revision has multiple tags, the most recent one (version sort) is used.
#   - For revisions that are NOT pointed to by a tag, the closest tag in lineage is used
#     with a suffix of `-<N>-g<SHA>` where `N` is the number of commits between HEAD and
#     that last tag and `SHA` is the commit sha. The `g` stands for `git`.
# - If git is NOT available the contents of the `VERSION` file in the root of the source tree
#   is used.
#   The `VERSION` file is maintained by release-tools (https://gitlab.com/gitlab-org/release-tools/).
#   For any kinds of releases it is expected that release-tools takes care of updating it
#   to the proper version before building the releases.
#
# The `BUILD_REF` is populated by the HEADs commit SHA if Git is available,
# or the value of `VERSION` is used otherwise.
GIT_TAG=$(git describe --tags --match v* 2>/dev/null || (test -f ./VERSION && echo "v$(cat ./VERSION)" || echo unknown))
GIT_COMMIT=$(git rev-parse --short HEAD 2>/dev/null || echo "$GIT_TAG")

# BUILD_TIME is a date time formatted like '2006-08-14T02:34:56-06:00'
# which is both ISO8601 and RFC3339 compatible.
BUILD_TIME=$(date -u -Iseconds)

# Prefix with STABLE_ so that these values are saved to stable-status.txt
# instead of volatile-status.txt.
# Stamped rules will be retriggered by changes to stable-status.txt, but not by
# changes to volatile-status.txt.
# See https://docs.bazel.build/versions/master/user-manual.html#flag--workspace_status_command
# DO NOT CHANGE ORDER OR ADD ANYTHING HERE without also adjusting column numbers in the "version" target in the makefile.
cat <<EOF
STABLE_BUILD_GIT_TAG ${GIT_TAG-}
STABLE_BUILD_GIT_COMMIT ${GIT_COMMIT-}
BUILD_TIME ${BUILD_TIME-}
EOF
