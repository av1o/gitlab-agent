#!/usr/bin/env bash

set -eu

eval $(minikube docker-env)

docker buildx build --build-arg "UBI_IMAGE=harbor.dcas.dev/registry.gitlab.com/gitlab-org/build/cng/gitlab-base:v17.0.0" --build-arg 'BUILDER_IMAGE=harbor.dcas.dev/docker.io/gitlab/gitlab-agent-ci-image:latest' --platform 'linux/amd64' --file build/agentk.ubi8-fips.Dockerfile --tag 'registry.gitlab.com/av1o/gitlab-agent/agentk:devel' .

cd build/deployment/gitlab-agent
kubectl kustomize | kpt live apply - --reconcile-timeout=2m --install-resource-group --server-side --show-status-events